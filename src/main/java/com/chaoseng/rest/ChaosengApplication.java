package com.chaoseng.rest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@ComponentScan(value = "com.chaoseng")
@EnableJpaRepositories("com.chaoseng.data")
@EntityScan(basePackages = "com.chaoseng.data.entities")
public class ChaosengApplication {

	public static void main(String[] args) {
		SpringApplication.run(ChaosengApplication.class, args);
	}

}
