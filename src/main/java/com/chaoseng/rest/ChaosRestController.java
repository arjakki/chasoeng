package com.chaoseng.rest;

import com.chaoseng.data.dto.UserDTO;
import com.chaoseng.services.intf.UserServiceInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/chaosapp")
@CrossOrigin(origins = "http://Web-NLB-T-151622647b2fa891.elb.us-east-1.amazonaws.com")
public class ChaosRestController {

    @Autowired
    UserServiceInterface userServiceInterface;
    @GetMapping("/name")
    public List<UserDTO> getChaonsName()
    {
        return userServiceInterface.getUserName();
    }
    @GetMapping("/addUser")
    public  String addUser()
    {
        UserDTO dto = new UserDTO();
        dto.setName("Sam");
        dto.setEmail("same@gmail.com");
        userServiceInterface.addUser(dto);

        UserDTO dto1 = new UserDTO();
        dto1.setName("Jhon");
        dto1.setEmail("john@gmail.com");
        userServiceInterface.addUser(dto1);

        UserDTO dto2 = new UserDTO();
        dto2.setName("create");
        dto2.setEmail("create@gmail.com");
        userServiceInterface.addUser(dto2);
        return "Added User";

    }

    @GetMapping("/healthcheck")
    @ResponseStatus(HttpStatus.OK)
    public void healthCheck()
    {
        return ;
    }

}
