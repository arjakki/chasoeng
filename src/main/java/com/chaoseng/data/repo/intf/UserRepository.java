package com.chaoseng.data.repo.intf;

import com.chaoseng.data.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;


public interface UserRepository extends JpaRepository<User,Integer> {
}
