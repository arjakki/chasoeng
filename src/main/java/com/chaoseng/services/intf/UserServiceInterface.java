package com.chaoseng.services.intf;

import com.chaoseng.data.dto.UserDTO;

import java.util.List;

public interface UserServiceInterface {

    public abstract List<UserDTO> getUserName();

    public abstract void addUser(UserDTO dto);
}
