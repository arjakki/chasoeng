package com.chaoseng.services.impl;

import com.chaoseng.data.dto.UserDTO;
import com.chaoseng.data.entities.User;
import com.chaoseng.data.repo.intf.UserRepository;
import com.chaoseng.services.intf.UserServiceInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component
@ComponentScan(value = "com.chaoseng")
public class UserService implements UserServiceInterface {

    @Autowired
    UserRepository userRepository;
    @Override
    public List<UserDTO> getUserName() {
        List<User> users = userRepository.findAll();
        List<UserDTO> userDtos = new ArrayList<>();
        if(users!=null && users.size() > 0) {
            for(User user : users) {
                UserDTO userDTO = new UserDTO();
                userDTO.setEmail(user.getEmail());
                userDTO.setId(user.getId());
                userDTO.setName(user.getName());
                userDtos.add(userDTO);
            }
        }
        return userDtos;
    }

    @Override
    public void addUser(UserDTO dto) {
        User user = new User();
        user.setEmail(dto.getEmail());
        user.setName(dto.getName());
        userRepository.save(user);
    }
}